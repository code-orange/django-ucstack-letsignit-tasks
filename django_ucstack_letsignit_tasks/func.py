import csv


def get_ps_ad_property_names():
    ps_ad_property_names = dict()

    with open("iam_directory_schema/ad-property-names.csv") as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")

        for row in csv_reader:
            ps_ad_property_names[row["Value"]] = row["Key"]

    return ps_ad_property_names
