import json

import ldap3
from celery import shared_task
from django_python3_ldap.conf import settings as ldap_settings

from django_ucstack_letsignit_client.django_ucstack_letsignit_client.func import *
from django_ucstack_letsignit_tasks.django_ucstack_letsignit_tasks.func import (
    get_ps_ad_property_names,
)
from django_ucstack_models.django_ucstack_models.models import UcTenantSettings


@shared_task(name="ucstack_letsignit_user_sync")
def ucstack_letsignit_user_sync():
    letsignit_tenants = UcTenantSettings.objects.filter(letsignit_enabled=True)

    ps_ad_property_names = get_ps_ad_property_names()

    # Configure the connection.
    if ldap_settings.LDAP_AUTH_USE_TLS:
        auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
    else:
        auto_bind = ldap3.AUTO_BIND_NO_TLS

    ldap_connection = ldap3.Connection(
        ldap3.Server(
            ldap_settings.LDAP_AUTH_URL,
            allowed_referral_hosts=[("*", True)],
            get_info=ldap3.NONE,
            connect_timeout=ldap_settings.LDAP_AUTH_CONNECT_TIMEOUT,
        ),
        user=ldap_settings.LDAP_AUTH_CONNECTION_USERNAME,
        password=ldap_settings.LDAP_AUTH_CONNECTION_PASSWORD,
        auto_bind=auto_bind,
        raise_exceptions=True,
        receive_timeout=ldap_settings.LDAP_AUTH_RECEIVE_TIMEOUT,
    )

    for letsignit_tenant in letsignit_tenants:
        api_token = lsi_fetch_token(
            letsignit_tenant.uctenantsettingsletsignit.lsi_app_id,
            letsignit_tenant.uctenantsettingsletsignit.lsi_app_secret,
        )

        api_session = lsi_start_session(api_token)

        users = list()

        if not ldap_connection.search(
            search_base="OU="
            + letsignit_tenant.customer.org_tag
            + ","
            + settings.HOSTING_LDAP_SEARCH_BASE,
            search_filter="(&(objectClass=user)(objectCategory=person))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        for user in ldap_connection.response:
            user_attributes_cleaned = dict()

            user_attributes_normalized = json.loads(
                json.dumps(
                    dict(user["attributes"]), default=lambda o: "<not serializable>"
                )
            )

            for (
                user_attribute_key,
                user_attribute_value,
            ) in user_attributes_normalized.items():
                if user_attribute_key in ps_ad_property_names:
                    user_attribute_key = ps_ad_property_names[user_attribute_key]

                if len(user_attribute_value) != 1:
                    user_attributes_cleaned[user_attribute_key] = user_attribute_value
                else:
                    user_attributes_cleaned[user_attribute_key] = user_attribute_value[
                        0
                    ]

            users.append(user_attributes_cleaned)

        lsi_users_response = lsi_push_data(api_token, api_session, "users", users)

        groups = list()

        if not ldap_connection.search(
            search_base="OU="
            + letsignit_tenant.customer.org_tag
            + ","
            + settings.HOSTING_LDAP_SEARCH_BASE,
            search_filter="(objectClass=group)",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        for group in ldap_connection.response:
            group_attributes_cleaned = dict()

            user_attributes_normalized = json.loads(
                json.dumps(
                    dict(group["attributes"]), default=lambda o: "<not serializable>"
                )
            )

            for (
                group_attribute_key,
                group_attribute_value,
            ) in user_attributes_normalized.items():
                if group_attribute_key in ps_ad_property_names:
                    group_attribute_key = ps_ad_property_names[group_attribute_key]

                if len(group_attribute_value) != 1:
                    group_attributes_cleaned[group_attribute_key] = (
                        group_attribute_value
                    )
                else:
                    group_attributes_cleaned[group_attribute_key] = (
                        group_attribute_value[0]
                    )

            groups.append(group_attributes_cleaned)

        lsi_groups_response = lsi_push_data(api_token, api_session, "groups", groups)

    return
