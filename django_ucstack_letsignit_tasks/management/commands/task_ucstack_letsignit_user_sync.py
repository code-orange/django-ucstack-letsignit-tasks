from django.core.management.base import BaseCommand

from django_ucstack_letsignit_tasks.django_ucstack_letsignit_tasks.tasks import (
    ucstack_letsignit_user_sync,
)


class Command(BaseCommand):
    help = "Run task: ucstack_letsignit_user_sync"

    def handle(self, *args, **options):
        ucstack_letsignit_user_sync()
